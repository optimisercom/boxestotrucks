/*********************************************
 * OPL 12.9.0.0 Model
 * Author: kafechew
 * Creation Date: 25 Aug 2019 at 8:28:14 PM
 *********************************************/

// Parameters
int nBoxes = ...;
int capacityTruckX = ...;
int capacityTruckY = ...;
float costTruckX = ...;
float costTruckY = ...;

// Decision Variables
dvar int+ truckX;
dvar int+ truckY; 

// Objective Function
minimize costTruckX * truckX + costTruckY * truckY;

// Constraints
subject to {
	MinimumCapacityforBoxes: // name of the constraint
	capacityTruckX * truckX + capacityTruckY * truckY >= nBoxes;
}